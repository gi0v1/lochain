# LoChain

### A simple blockchain written in Rust.

## Block structure
| Block               |
|---------------------|
| Transactions        |
| Previous block hash |
| Current block hash  |
| Nonce               |
| ID                  |

## Features

- [x] Simple transaction list
- [x] Simple block struct
- [x] Simple block chain struct
- [ ] More advanced block stucture
- [ ] Networked
- [ ] Mining difficulty increase / decrease algorithm
- [ ] Decentralization

## Note
This is my first Rust project, so don't bully me for my bad code.
