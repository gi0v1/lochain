pub mod lo_block {
    use crate::hashing::sha256::block_digest;

    #[derive(Debug)]
    pub struct LoTransaction {
	pub src_addr: u64,
	pub dst_addr: u64,
	pub value_tx: u32,
    }

    #[derive(Debug)]
    pub struct LoBlock {
	pub transactions: Vec<LoTransaction>,
	pub prev_block_hash: String,
	pub curr_block_hash: String,
	pub block_nonce: u64,
	pub block_id: u32
    }

    pub fn init_lo_transaction(src: u64, dst: u64, val: u32) -> LoTransaction {
	LoTransaction {
	    src_addr: src,
	    dst_addr: dst,
	    value_tx: val
	}
    }

    pub fn init_lo_block(pbh: String, id: u32, transs: Vec<LoTransaction>) -> LoBlock {
	let mut block = LoBlock {
	    block_id: id,
	    block_nonce: 0,
	    transactions: transs,
	    prev_block_hash: pbh,
	    curr_block_hash: "".to_string()
	};

	for nonce in 0..u64::MAX {
	    block.block_nonce = nonce;

	    if validate_block_digest(block_digest(&mut block)) { break; }
	}

	block.curr_block_hash = block_digest(&mut block);

	return block;
    }

    pub fn validate_block_digest(digest: String) -> bool {

	for i in 0..5 {
	    if digest.as_bytes()[i] != ('0' as u8) {
		return false;
	    }
	}

	return true;
    }
}
