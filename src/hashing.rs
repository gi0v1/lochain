pub mod sha256 {
    use sha2::{Sha256, Digest};
    use crate::block::lo_block::LoBlock;

    pub fn sha256_digest(string: &str) -> String {
	let mut hasher = Sha256::new();

	hasher.update(string.as_bytes());

	return format!("{:X}", hasher.finalize());
    }

    pub fn block_digest(block: &mut LoBlock) -> String {
	let mut to_digest = "".to_string();

	for transaction in &block.transactions {
	    to_digest.push_str(&transaction.src_addr.to_string());
	    to_digest.push_str(&transaction.dst_addr.to_string());
	    to_digest.push_str(&transaction.value_tx.to_string());
	}

	to_digest.push_str(&block.prev_block_hash);
	to_digest.push_str(&block.block_id.to_string());
	to_digest.push_str(&block.block_nonce.to_string());

	return sha256_digest(&to_digest);
    }
}
