pub mod lo_block_chain {
    use crate::block::lo_block::LoBlock;
    use crate::block::lo_block::init_lo_transaction;
    use crate::block::lo_block::init_lo_block;

    #[derive(Debug)]
    pub struct LoBlockChain {
	pub blocks: Vec<LoBlock>,
	pub last_hash: String
    }

    pub fn init_lo_block_chain() -> LoBlockChain {
	let mut block_chain = LoBlockChain {
	    blocks: Vec::new(),
	    last_hash: "".to_string()
	};

	let mut transactions = Vec::new();

	transactions.push(init_lo_transaction(0, 0, 0));

	let genesis_block = init_lo_block((0..64).map(|_| "0").collect::<String>(), 0, transactions);

	add_block_to_chain(&mut block_chain, genesis_block);

        return block_chain;
    }

    pub fn add_block_to_chain(block_chain: & mut LoBlockChain, block: LoBlock) {
	block_chain.last_hash = block.curr_block_hash.clone();
	block_chain.blocks.push(block);
    }
}
