mod hashing;
mod block;
mod block_chain;

//use crate::hashing::sha256::sha256_digest;
use crate::block::lo_block::init_lo_block;
use crate::block::lo_block::init_lo_transaction;
use crate::block_chain::lo_block_chain::add_block_to_chain;
use crate::block_chain::lo_block_chain::init_lo_block_chain;

fn main() {
    let mut transactions = Vec::new();

    transactions.push(init_lo_transaction(0xAAAABBBBCCCCDDDD, 0xDDDDCCCCBBBBAAAA, 123));
    transactions.push(init_lo_transaction(0xAAAABBBBCCCCDDDD, 0xDDDDCCCCBBBBAAAA, 69));
    transactions.push(init_lo_transaction(0xAAAABBBBCCCCDDDD, 0xDDDDCCCCBBBBAAAA, 420));
    transactions.push(init_lo_transaction(0xAAAABBBBCCCCDDDD, 0xDDDDCCCCBBBBAAAA, 420));
    transactions.push(init_lo_transaction(0xAAAABBBBCCCCDDDD, 0xDDDDCCCCBBBBAAAA, 420));
    transactions.push(init_lo_transaction(0xAAAABBBBCCCCDDDD, 0xDDDDCCCCBBBBAAAA, 421));

    let mut block_chain = init_lo_block_chain();

    let block = init_lo_block(block_chain.last_hash.clone(), 1, transactions);

    add_block_to_chain(&mut block_chain, block);

    println!("{:#X?}", block_chain);
}
